import axios from 'axios';
import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Table } from 'reactstrap';
import './style.css';

export class ListProducts extends Component {
  static displayName = ListProducts.name;

  constructor(props) {
    super(props);
    this.state = {
      list: [],
      listVisible: false
    };
  }

  componentDidMount() {
    axios.get('/caritas/listar-produtos')
      .then(res => {
        this.setState({ list: res.data.data, listVisible: true });
      })
  }
  

  render () {
    const Results = () => (
      <div>
        <Table striped>
        <thead>
          <tr>
            <th>#</th>
            <th>Nome</th>
            <th>Valor</th>
          </tr>
        </thead>
        <tbody>
          { this.state.list.map(item => 
            <tr>
              <th scope="row">{item.id}</th>
              <td>{item.name}</td>
              <td>{item.value}</td>
            </tr>
          )}
        </tbody>
      </Table>
      </div>
    );

    return (
        <div>
          <Container fluid={true}>
            <Row>
              <Col md={12}>
              </Col>
            </Row>
            <Row>
              <Col md={12}>
                { this.state.listVisible ? <Results /> : null }
              </Col>
            </Row>
          </Container>
        </div>
    );
  }
}
