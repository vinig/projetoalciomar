import axios from 'axios';
import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Link } from "react-router-dom";
import Message from './Message';
import './style.css';

export class Client extends Component {
  static displayName = Client.name;

  constructor(props) {
    super(props);
    this.state = {
      id: this.props.location.state.id,
      name: this.name,
      price: this.price,
      cep: this.cep,
      address: this.address,
      Visible: false,
      visibleMessage: false,
      message: '',
      typeMessage: 'danger'
    };

    this.getList = this.getList.bind(this);
    this.NameChange = this.NameChange.bind(this);
    this.ValueChange = this.ValueChange.bind(this);
    this.sendSubmit = this.sendSubmit.bind(this);
    this.setMessage = this.setMessage.bind(this);
    this.setCep = this.setCep.bind(this);
  }

  componentDidMount() {
    this.setState({id: this.props.location.state.id});
    this.getList();
  }

  NameChange(event) {
    this.setState({name: event.target.value});
  }

  ValueChange(event) {
    this.setState({price: event.target.value.replace(".",",")});
  }

  sendSubmit(event) {
    const myObj = {
      id: this.state.id,
      name: this.state.name,
      cep: this.state.cep,
      address: this.state.address
    }

    let body = {
      obj: myObj,
      isDeleted: false 
    };

    axios.post("/caritas/criar-clientes", body)
    .then(function (response) {
      this.setMessage(response.data.message, response.data.error);
    }.bind(this))
    .catch(function (error) {
      this.setMessage(error.message, true);
    }.bind(this));
    event.preventDefault();
  }

  getList(){
    axios.get('/caritas/buscar-cliente/' + this.state.id)
    .then(res => {
      var price = res.data.data.value;
      this.setState({ name: res.data.data.name, price: price, cep: res.data.data.cep, address: res.data.data.address, Visible: true });
    }).catch(err => {
        this.setState({ name: '', price: '', cep: '', address: '', Visible: true });
      }
    );
  }

  setMessage(message, typeM){
    var type = typeM ? 'danger' : 'success';
    this.setState({ message: message, type: type , visibleMessage: true });
    setTimeout(function(){ window.location.href = '/painel'; }, 1500);
  }

  setCep(event){
    console.log(event.target.value);
    axios.post('https://viacep.com.br/ws/' + event.target.value + '/json')
    .then(res => {
      var price = res.data.data.value;
      this.setState({ cep: res.data.data.cep, address: res.data.data.logradouro });
    }).catch(err => {
        this.setState({ cep: '', address: '' });
      }
    );
  }
  
  render () {
    return (
        <div>
          <Container fluid={true} style={{marginTop: '5%'}}>
            <Row>
            <Col md={4}>
              </Col>
              <Col md={4}>
                <form onSubmit={this.sendSubmit} style={{displayName: ( this.state.Visible ? 'visible' : 'none')}}>
                  <label>Nome</label>
                  <div>
                    <input placeholder="Nome" type="text" className="form-control" value={this.state.name} onChange={this.NameChange} required />
                  </div>
                  {/* <br/>
                  <label>CEP</label>
                  <div>
                    <input placeholder="CEP" type="text" className="form-control" value={this.state.cep} onChange={this.setCep} required />
                  </div>
                  <br/>
                  <label>Logradouro</label>
                  <div>
                    <input placeholder="Logradouro" type="text" className="form-control" value={this.state.address} required />
                  </div> */}
                  <br/>
                  <center>
                    <Link to={"/painel"} className="btn btn-primary">Voltar</Link>
                    <input type="submit" className="btn btn-success" style={{marginLeft: '5%'}} value="Enviar" />
                  </center>
                </form>
                <br />
                <center>
                  <Message visible={this.state.visibleMessage} message={this.state.message} type={this.state.type} />
                </center>
              </Col>
            </Row>
          </Container>
        </div>
    );
  }
}
