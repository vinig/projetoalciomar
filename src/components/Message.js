import React from 'react';

const Message = (props) => (
    <div className={"alert alert-" + props.type} style={{ visibility: (props.visible ? "visible" : "hidden") }}>
        {props.message}
    </div>
);

export default Message;
