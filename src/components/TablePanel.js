import React from 'react';
import { Table } from 'reactstrap';
import { Link } from "react-router-dom";

const TablePanel = (props) => {
  const Results = () => (
    <div>
      <Table striped>
      <thead>
        <tr>
          <th>#</th>
          <th>Nome</th>
          <th>Ação</th>
        </tr>
      </thead>
      <tbody>
        { props.list.map(item => 
          <tr key={item.id}>
            <th scope="row">{item.id}</th>
            <td>{item.name}</td>
            <td><Link to={{ pathname: "/"+ props.type, state: { id : item.id } }} className="btn btn-success">Editar</Link> </td>
          </tr>
        )}
      </tbody>
    </Table>
    </div>
  );
  
  return (
    <div>
      { props.visible ? <Results /> : null }
    </div>
  );
}

export default TablePanel;