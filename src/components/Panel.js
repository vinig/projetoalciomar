import axios from 'axios';
import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Nav, NavItem, NavLink } from 'reactstrap';
import { Link } from "react-router-dom";
import Message from './Message';
import Menu from './Menu';
import TablePanel from './TablePanel';
import './style.css';

export class Panel extends Component {
  static displayName = Panel.name;

  constructor(props) {
    super(props);
    this.state = {
      list: [],
      listVisible: false,
      query: 'produtos'
    };

    this.setList = this.setList.bind(this);
    this.productClick = this.productClick.bind(this);
    this.clientClick = this.clientClick.bind(this);
  }

  productClick() {
    this.setState({ query: 'produtos' });
    this.setList('produtos');
  }

  clientClick() {
    this.setState({ query: 'clientes' });
    this.setList('clientes');
  }

  componentDidMount() {
    this.setList('produtos');
  }

  setList(query) {
    axios.get('/caritas/listar-'+query)
    .then(res => {
      this.setState({ list: res.data.data, listVisible: true });
    });
  }

  render () {
    return (
        <div>
          <Container fluid={true}>
            <Row>
              <Col md={12}>
                <Message visible={this.state.visible} message={this.state.message} type={'danger'} />
              </Col>
            </Row>
            <Row>
              <Col md={4}>
                <Nav vertical>
                  <NavItem>
                    <NavLink onClick={this.productClick}>Produtos</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink onClick={this.clientClick}>Clientes</NavLink>
                  </NavItem>
                </Nav>
              </Col>
              <Col md={7}>
                <Link to={{ pathname: "/" + this.state.query, state: { id : 0 } }} className="btn btn-primary">Novo</Link> 
                <TablePanel list={this.state.list} visible={this.state.listVisible} type={this.state.query} />
              </Col>
              <Col md={1}>
                <Link to={"/"} className="btn btn-danger">Sair</Link>
              </Col>
            </Row>
          </Container>
        </div>
    );
  }
}
