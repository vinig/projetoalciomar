import axios from 'axios';
import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import Message from './Message';
import './style.css';

export class Login extends Component {
  static displayName = Login.name;

  constructor(props) {
    super(props);
    this.state = {
        email: this.email,
        password: this.password,
        visible: this.visible,
        message: this.message
    };

    this.setMessage = this.setMessage.bind(this);
    this.sendLogin = this.sendLogin.bind(this);
    this.emailChange = this.emailChange.bind(this);
    this.passwordChange = this.passwordChange.bind(this);
  }

  emailChange(event) {
    this.setState({ email: event.target.value });
  }

  passwordChange(event) {
    this.setState({ password: event.target.value });
  }

  setMessage(message) {
    this.setState ({
      visible: true,
      message: message
    });
  }

  sendLogin(event) {
    let body = {
        email: this.state.email,
        pass: this.state.password
    };

    axios.post("/caritas/login", body)
    .then(function (response) {
        if(response.data.error === true)
          this.setMessage(response.data.message);
          else{
            window.location.href = '/painel';
          }
          // this.setMessage('oi acertou');
    }.bind(this))
    .catch(function (error) {
      this.setMessage(error.message);
    }.bind(this));
    event.preventDefault();
  }

  render () {
    return (
      
      <div id="wrapper">
        <div id="login" className="animate form">
          <div className="login_content">
            <Container fluid={true}>
              <Row>
                <Col md={12}>
                  <Message visible={this.state.visible} message={this.state.message} type={'danger'} />
                </Col>
              </Row>
              <Row>
                <Col md={4}></Col>
                <Col md={4}>
                  <form onSubmit={this.sendLogin}>
                    <h1>Caritas</h1>
                    <div>
                      <input type="text" value={this.email} onChange={this.emailChange} className="form-control" placeholder="Usuário" required />
                    </div>
                    <br/>
                    <div>
                      <input type="password" value={this.password} onChange={this.passwordChange} className="form-control" placeholder="Senha" required />
                    </div>
                    <br/>
                    <div>
                      <center><button type="submit" className="btn btn-secondary">Entrar</button></center>
                    </div>
                  </form>    
                </Col>
              </Row>
            </Container>
          </div>
        </div>
      </div>
    );
  }
}
