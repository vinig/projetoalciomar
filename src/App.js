//import { Route } from 'react-router';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import React, { Component } from 'react';
import { Layout } from './components/Layout';
import { Login } from './components/Login';
import { Panel } from './components/Panel';
import { ListProducts } from './components/ListProducts';
import { Product } from './components/Product';
import { Client } from './components/Client';

/*import { Redaction } from './components/Redaction';
//import { NotFound } from './components/NotFound';
import '../src/styles/app.css';*/

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
            <Route exact path='/' component={Login} />
            <Route path='/painel' component={Panel} />
            <Route path='/valores' component={ListProducts} />
            <Route path='/produtos' component={Product} />
            <Route path='/clientes' component={Client} />
            {/*<Route path='/painel' component={Panel} />
            <Route path='/corrigir' component={Redaction} />
            <Route path='*' component={NotFound} />*/}
      </Layout>
    );
  }
}
